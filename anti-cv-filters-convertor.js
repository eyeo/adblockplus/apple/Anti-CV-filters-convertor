/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Advanced coversion of filters by including also filters not convertible to WebKit format.
   Output format:
{
  "snippets": {
    target_domain_name: [
       {
            snippet_function_name: [
                snippet_function_parameter
            ] // snippet function parameters
       },
       ...
    ],
    ...
  },
  "element_hiding_emulation": {
     target_domain_name: [
       {
           "text": raw_filter_tet
           "selector": selector_text
       },
      ...
    ],
    ...
  }
}
*/
"use strict";

const {Filter, SnippetFilter, ElemHideEmulationFilter} = require("adblockpluscore/lib/filterClasses");
const {SnippetsList} = require("./lib/snippets")
const {ElementHidingEmulationList} = require("./lib/element-hiding-emulation")

let readline = require("readline");
let rl = readline.createInterface({input: process.stdin, terminal: false});
let snippetsList = new SnippetsList();
let eheList = new ElementHidingEmulationList();

rl.on("line", line =>
{
  if (/^\s*[^\[\s]/.test(line)) {
    let filter = Filter.fromText(Filter.normalize(line))
    if (filter instanceof SnippetFilter) {
      snippetsList.addFilter(filter);
    } else if (filter instanceof ElemHideEmulationFilter)  {
      eheList.addFilter(filter)
    } else {
      // The rest of filters are handled by https://gitlab.com/eyeo/adblockplus/apple/abp2blocklist
    }
  } 
});

rl.on("close", () =>
{
    let eheFilters = eheList.generateList();
    let snippetFilters = snippetsList.generateList();
    console.log("{");
    // console.group();
    console.log(JSON.stringify("snippets", null, "\t") +
                ": " +
                JSON.stringify(snippetFilters, null, "\t") +
                ",");
    console.log(JSON.stringify("element_hiding_emulation", null, "\t") + 
                ": " + 
                JSON.stringify(eheFilters, null, "\t")
                );
    // console.group();
    // console.groupEnd();
    // console.log("]");
    // console.groupEnd()
    console.log("}");

    console.error("🔢 Generated:");
    console.error("Element hide emulation filters for " + Object.keys(eheFilters).length + " domains");
    console.error("Snippet filters for " + Object.keys(snippetFilters).length + " domains");
});
