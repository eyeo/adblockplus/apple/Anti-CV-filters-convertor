/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

const {SnippetsList} = require("../lib/snippets.js");
const {Filter} = require("adblockpluscore/lib/filterClasses");
"use strict";

describe("Snippets conversion", () =>
{
    function compareFilters(filters, expected) {
        let snippetsList = new SnippetsList();
        for(rawFilter of filters) {
            let snippet = Filter.fromText(rawFilter);
            snippetsList.addFilter(snippet);
        }
        let result = snippetsList.generateList();
        expect(result).toEqual(expected)
    }

    test("No snippets", (done) => {
        let snippetsList = new SnippetsList();
        let result = snippetsList.generateList();
        expect(result).toEqual({});
        done()
    })

    test("One domain, one function, one parameter", (done) => {
        compareFilters(
            ["spankwire.com#$#abort-on-property-write ppAb"],
            { 
                "spankwire.com": [ 
                    { 
                        "abort-on-property-write": [
                            "ppAb"
                        ] 
                    } 
                ] 
            }
        )
        done()
    })

    test("One domain, one function, multiple parameters", (done) => {
        compareFilters(
            ["yandex.com#$#hide-if-contains ad li.serp-item"],
            { 
                "yandex.com": [ 
                    { 
                        "hide-if-contains": [
                            "ad", "li.serp-item"
                        ] 
                    } 
                ] 
            }
        )
        done()
    })

    test("One domain, one function, multiple parameters with quotes", (done) => {
        compareFilters(
            ["yandex.com#$#hide-if-contains ad li.serp-item 'li.serp-item div.label'"],
            { 
                "yandex.com": [ 
                    { 
                        "hide-if-contains": [
                            "ad", "li.serp-item", "'li.serp-item div.label'"
                        ] 
                    } 
                ] 
            }
        )
        done()
    })

    test("One domain, multiple functions, multiple parameters", (done) => {
        compareFilters(
            ["hdvid.tv#$#abort-on-property-read atob; abort-on-property-read encodeURIComponent; abort-on-property-write _pop"],
            { 
                "hdvid.tv": [ 
                    { 
                        "abort-on-property-read": [
                            "atob"
                        ]
                    },
                    {
                        "abort-on-property-read": [
                            "encodeURIComponent"
                        ]
                    },
                    {
                        "abort-on-property-write": [
                            "_pop"
                        ]
                    } 
                ] 
            }
        )
        done()
    })

    test("Multiple domains, multiple functions, multiple parameters", (done) => {
        compareFilters(
            ["hdvid.tv,yandex.com#$#abort-on-property-read atob; abort-on-property-read encodeURIComponent; abort-on-property-write _pop"],
            { 
                "hdvid.tv": [ 
                    { 
                        "abort-on-property-read": [
                            "atob"
                        ]
                    },
                    {
                        "abort-on-property-read": [
                            "encodeURIComponent"
                        ]
                    },
                    {
                        "abort-on-property-write": [
                            "_pop"
                        ]
                    } 
                ],
                "yandex.com": [ 
                    { 
                        "abort-on-property-read": [
                            "atob"
                        ]
                    },
                    {
                        "abort-on-property-read": [
                            "encodeURIComponent"
                        ]
                    },
                    {
                        "abort-on-property-write": [
                            "_pop"
                        ]
                    } 
                ]  
            }
        )
        done()
    })

    test("Multiple domains with excluded domain, multiple functions, multiple parameters", (done) => {
        compareFilters(
            ["hdvid.tv,~yandex.com#$#abort-on-property-read atob; abort-on-property-read encodeURIComponent; abort-on-property-write _pop"],
            { 
                "hdvid.tv": [ 
                    { 
                        "abort-on-property-read": [
                            "atob"
                        ]
                    },
                    {
                        "abort-on-property-read": [
                            "encodeURIComponent"
                        ]
                    },
                    {
                        "abort-on-property-write": [
                            "_pop"
                        ]
                    } 
                ] 
            }
        )
        done()
    })

    test("Same domain in multiple snippet filters", (done) => {
        compareFilters(
            ["www.youtube.com,m.youtube.com#$#override-property-read playerResponse.adPlacements undefined",
             "www.youtube.com#$#json-prune '0.playerResponse.adPlacements 0.playerResponse.playerAds'"
            ],
            { 
                "www.youtube.com": [ 
                    { 
                        "override-property-read": [
                            "playerResponse.adPlacements", "undefined"
                        ]
                    },
                    { 
                        "json-prune": [
                            "'0.playerResponse.adPlacements 0.playerResponse.playerAds'"
                        ]
                    }
                ],
                "m.youtube.com": [ 
                    { 
                        "override-property-read": [
                            "playerResponse.adPlacements", "undefined"
                        ] 
                    } 
                ]
            }
        )
        done()
    })
})