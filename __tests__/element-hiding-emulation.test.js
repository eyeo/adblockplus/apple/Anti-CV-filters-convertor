/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

const {ElementHidingEmulationList} = require("../lib/element-hiding-emulation.js");
const {Filter} = require("adblockpluscore/lib/filterClasses");
"use strict";

describe("Element hiding conversion", () =>
{
    function compareFilters(filters, expected) {
        let eheList = new ElementHidingEmulationList();
        for(rawFilter of filters) {
            let eheFilter = Filter.fromText(rawFilter);
            eheList.addFilter(eheFilter);
        }
        let result = eheList.generateList();
        expect(result).toEqual(expected)
    }

    test("No filters", (done) => {
        compareFilters([], {})
        done()
    })

    test("Single domain", (done) => {
        compareFilters(
            [
                "horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))"
            ],
            {
                "horoskopy.cz": [
                    {
                        "text": "horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    }
                ]
            }
        )
        done()
    })

    test("Multiple domains", (done) => {
        compareFilters(
            [
                "sport.cz,seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))"
            ],
            {
                "sport.cz": [
                    {
                        "text": "sport.cz,seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    }
                ],
                "seznamzpravy.cz": [
                    {
                        "text": "sport.cz,seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    }
                ],
                "horoskopy.cz": [
                    {
                        "text": "sport.cz,seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    }
                ]
            }
        )
        done()
    })

    test("Multiple domains with excluded domain", (done) => {
        compareFilters(
            [
                "sport.cz,seznamzpravy.cz,~horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))"
            ],
            {
                "sport.cz": [
                    {
                        "text": "sport.cz,seznamzpravy.cz,~horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    }
                ],
                "seznamzpravy.cz": [
                    {
                        "text": "sport.cz,seznamzpravy.cz,~horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    }
                ]
            }
        )
        done()
    })

    test("Multiple ehe filter for the same domain", (done) => {
        compareFilters(
            [
                "seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                "horoskopy.cz#?#li[class*=\"display-\"]:-abp-has(div > h3 + p:-abp-contains(Ad: ))"
            ],
            {
                "seznamzpravy.cz": [
                    {
                        "text": "seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    }
                ],
                "horoskopy.cz": [
                    {
                        "text": "seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
                        "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
                    },
                    {
                        "text": "horoskopy.cz#?#li[class*=\"display-\"]:-abp-has(div > h3 + p:-abp-contains(Ad: ))",
                        "selector": "li[class*=\"display-\"]:-abp-has(div > h3 + p:-abp-contains(Ad: ))"
                    },
                ]
            }
        )
        done()
    })
})