/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @fileOverview Definition of ElementHidingEmulationList suitable for usage in ABPKit.
 */

const {extractDomains} = require("./domains.js");

let ElementHidingEmulationList =
/**
 * Create a new SnippetsList
 *
 *
 * @constructor
 */
 exports.ElementHidingEmulationList = function()
 {
   this.filters = [];
 };

/**
 * Add Adblock Plus snippet filter
 *
 * @param {filterClasses.SnippetFilter} snippet filter to be added
 */
ElementHidingEmulationList.prototype.addFilter = function(filter) 
{
    this.filters.push(filter)
}

/**
 * Generate element hiding emulation list from the added filters
 *
 * @returns A json with the parsed EHE filters in the format appropriate for ABPKit:
  {
    target_domain_name: [
       {
           "text": raw_filter_tet
           "selector": selector_text
       },
      ...
    ], // The EHE filters applicable on the given domain
    ...
  } // A list of domains with related EHE filters

 * Example:

  Raw EHE filter:
  "sport.cz,seznamzpravy.cz,~horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))"

  will be parsed to:
  {
      "sport.cz": [
          {
              "text": "sport.cz,seznamzpravy.cz,horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
              "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
          }
      ],
      "seznamzpravy.cz": [
          {
              "text": "sport.cz,seznamzpravy.cz,~horoskopy.cz#?#div:-abp-has(>p:-abp-contains(Reklama))",
              "selector": "div:-abp-has(>p:-abp-contains(Reklama))"
          }
      ]
  }
*/
ElementHidingEmulationList.prototype.generateList = function()
{
  let list = new Object();
  for(const eheFilter of this.filters) {
    // Filter out empty domains, core adds an additional empty domain,
    // not needed for this use case
    let domains = extractDomains(eheFilter);
    let filterData = {"text": eheFilter.text, "selector": eheFilter.selector};

    for(const domain of domains) {
      if(list.hasOwnProperty(domain)) {
          list[domain].push(filterData);
      } else {
          list[domain] = [filterData];
      }
    }
  }
  return list;
}