/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @fileOverview Definition of SnippetsList suitable for usage in ABPKit.
 */

 const {extractDomains} = require("./domains.js");

// Snippet function arguments are split by a space, but some arguments
// are enclosed in single quotes which may contain spaces on their own.
//
// For example: hide-if-contains ad li.serp-item 'li.serp-item div.label'
// Has the next arguments: ad, li.serp-item and 'li.serp-item div.label'
//
// Based on:
// (?:         # non-capturing group
//    [^\s']+  # anything that's not a space or a quote
//    |        #   or…
//    '        # opening quote
//      [^']+  # …followed by one or more characters that are not a quote
//    '        # …closing quote
//  )+
let argsSplitRegex = /(?:[^\s']+|'[^']*')+/g;

let SnippetsList =
/**
 * Create a new SnippetsList
 *
 *
 * @constructor
 */
exports.SnippetsList = function()
{
  this.snippets = [];
};

/**
 * Add Adblock Plus snippet filter
 *
 * @param {filterClasses.SnippetFilter} snippet filter to be added
 */
SnippetsList.prototype.addFilter = function(filter) {
    this.snippets.push(filter)
}

/**
 * Generate snippets list from the added filters
 *
 * @returns A json with the parsed snippets in the format appropriate for ABPKit:
  {
    target_domain_name: [
       {
            snippet_function_name: [
                snippet_function_argument
            ] // Arguments applicable for the given snippet
       }, // Snippet function described by its name and its arguments
       ...
    ], // The snippets applicable on the given domain
    ...
  } // A list of domains with related snippet functions

 * Example:

  Raw snippet filter:
  "hdvid.tv#$#abort-on-property-read atob; abort-on-property-read encodeURIComponent; abort-on-property-write _pop"

  will be parsed to:
  { 
    "hdvid.tv": [ 
        { 
            "abort-on-property-read": [
                "atob"
            ]
        },
        {
            "abort-on-property-read": [
                "encodeURIComponent"
            ]
        },
        {
            "abort-on-property-write": [
                "_pop"
            ]
        } 
    ] 
   }
*/
SnippetsList.prototype.generateList = function() {
    let list = new Object();
    for(const snippet of this.snippets) {
        let domains = extractDomains(snippet);
        let script = snippet.body;

        // Example: abort-on-property-read atob; abort-on-property-read encodeURIComponent; abort-on-property-write _pop
        let rawCommands = script.split(';');
        let parsedCommands = [];
        for(var rawCommand of rawCommands) {
            // Example of rowCommand - 'hide-if-contains ad li.serp-item'
            rawCommand = rawCommand.trim();

            // First word is the function name
            let functionEndIndex = rawCommand.indexOf(' ');
            let snippetFunction = rawCommand.substr(0, functionEndIndex);

            // The rest of the string contains the function argument
            let args = rawCommand.substr(functionEndIndex + 1).match(argsSplitRegex);
            parsedCommands.push({ [snippetFunction]: args });
        }

        for(const domain of domains) {
            if(list.hasOwnProperty(domain)) {
                list[domain] = list[domain].concat(parsedCommands);
            } else {
                list[domain] = parsedCommands;
            }
        }
    }

    return list;
}