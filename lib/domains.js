/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Extracts the domains from a given filter.
 * @param {filterClasses.Filter} filter to extract domains from
 * 
 * @returns A list of domains as strings. domains of from '~domain' and empty domains are removed. 
 */
function extractDomains(filter) {
    let extractedDomains = [];

    // @type {?Map.<string,boolean>}
    let domains = filter.domains

    domains.forEach((isIncluded, domain) => {
        if(isIncluded == true && domain.length > 0) {
            extractedDomains.push(domain);
        }
    });

    return extractedDomains;
}

module.exports = {
    extractDomains
};