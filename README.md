# Anti CV Filters Convertor

Converts Anti CV filters to appropriate format to be used by ABPKit.

## Requirements

The required packages can be installed via [NPM](https://npmjs.org):

```
npm install
```

## Usage

Create a WebKit block list `output.json` from the Adblock Plus filter list `input.txt`:
```
node anti-cv-filters-convertor.js < input.txt > output.json
```

## Tests

Unit tests live in the `__tests__/` directory. To run the unit tests ensure you have
already installed the required packages (see above) and then type this command:

```
npm test
```